/* 
  generate an array A[] of length 1000 by generating each elements by using random integer generator in range [0,1000]. Again generate
  a random integer "n" and use sequentialSearch method to locate "n" in A[]. Calculate number of steps to find "n".

  * Repeat the process enclosed in {} 20 times for different "n"
      -> What is the minimum time taken?
      -> What is the maximum time taken?
      -> What is the average time taken?
*/


#include <iostream>
#include<chrono>
#include<map>
typedef long long unsigned llu;
using namespace std;
 
int sequentialSearch(int *arr, int randomNumber) {
  
  for(int i=0; i < 1000; i++) {
    if(randomNumber==arr[i]) 
      return i+1;
  }
  return -1;
}

int main(){
    int lb = 1, ub = 1000;
    int *arr = new int[1000];
    std::map<int,llu> timeTaken;

   /* for generating array of length 1000 in range [1,1000]  */
    srand(time(0));
    for (int i = 0; i < 1000; i++)
        arr[i]= (rand() % (ub - lb + 1)) + lb;

    cout << "The array is : \n"; 
    for(int i=0;i<1000; i++)
      cout << arr[i] << " ";
    
    cout << endl<<endl;

    for(int i=0; i<20; ++i) {
      /* generate a random number */
      int randomNumber = (rand() % (ub - lb + 1)) + lb;
      cout<< "\n\n-------[time to search the random number " << randomNumber << " ]-------"<<endl; 
      chrono::time_point<chrono::system_clock> start,end;
      start = chrono::system_clock::now();
      int pos = sequentialSearch(arr,randomNumber);
      end = chrono::system_clock::now();

      if (pos!=-1) 
        // cout << "\033[1;32m[+] The element is found at position " << i+1 << "\033[0m" << endl;
        cout << "[+] The element is found at position " << i+1 << endl;
      else
        cout << "[x] The element not found" << endl;
        // cout << "\033[1;31m[x] The element not found\033[0m" << endl;

      chrono::duration<llu,std::nano> elapsed_seconds = end - start;
      cout << "[!] Elapsed time for recursive : " << elapsed_seconds.count() << "ns\n";
      timeTaken.insert(pair<int, llu>(randomNumber,elapsed_seconds.count()));
    }
    
    map<int,llu>::iterator it;
//    cout << "\n\nThe map is: \n";
//    cout << "\tRandom number \t     Elapsed time" << endl;
//    for(it=timeTaken.begin(); it!=timeTaken.end(); ++it) {
//      cout <<"\t    " << it->first << "\t\t\t" << it->second << "ns" << endl;
//    }
    it = timeTaken.begin();
    llu minimum = it->second;

    int miniNum,maxNum;
    llu maximum = it->second;
    float total =0;
    for(auto i=++it; it!=timeTaken.end(); ++it) {
      if(it->second < minimum) {
        miniNum = it->first;
        minimum = it->second;
      }
      if(it->second > maximum) {
        maxNum = it->first;
        maximum = it->second;
      }
      total += it->second; 

    }
    cout << "\n\nminimum time elapsed = " << minimum << "ns for " <<miniNum <<endl;
    cout << "maximum time elapsed = " << maximum << "ns for " << maxNum <<endl;
    cout << "average time elapsed = " << total/20.0  << "ns" << endl;
    return 0;
}

