#include <iostream>
#include <algorithm>
using namespace std;

int steps=0;
struct Jobs {
    char id;
    int profit;
    int deadline;
    Jobs(char id,int deadline, int profit){
        this->id = id;
        this->deadline = deadline;
        this-> profit = profit;
    }
};

bool sortProfit(Jobs a,Jobs b) {
    return a.profit > b.profit;
}

int min(int num1, int num2) {
    return (num1 < num2) ? num1 : num2 ;
}

void jobSequencing(Jobs arr[], int n) {
    
    sort(arr,arr+n,sortProfit);

    int result[n];
    bool slot[n];    // for tracking free slots

    // initialize all the slots to be free
    for(int i=0;i<n; i++){
        slot[i] = false;
    }

    int profit=0;
    // traverse all the given jobs
    for(int i=0; i<n; i++){
        // finding free slot( last possible slot)
        for(int j= min(n,arr[i].deadline)-1; j>=0; j--){
            steps +=1;
            // if free slot found
            if(slot[j] == false){
                profit+=arr[j].profit;
                result[j] = i;
                slot[j] = true;
                break;
            }
        } 
    }

    cout << "The optimal profit = " << profit << " with processing sequence: {";
    // print the result
    for(int i=0; i < n; i++) {
        if(slot[i])
            cout<<arr[result[i]].id << ", " ;
    }

    cout <<"}" << endl;
}

void printJobs(Jobs arr[],int size) {
    for(int i=0;i<size; i++) {
        cout << i+1 << " : " << endl;
        cout <<"\t id       : "<<arr[i].id << endl;
        cout <<"\t profit   : "<<arr[i].profit<< endl;
        cout <<"\t deadline : "<<arr[i].deadline<< endl;
    }
    cout << endl;
}

int main(){
    Jobs arr[] = {
                { 'a', 2, 100 },
                { 'b', 1, 19 },
                { 'c', 2, 27 },
                { 'd', 1, 25 },
                { 'e', 3, 15 }
    };
    int n = sizeof(arr) / sizeof(arr[0]);
    
//    cout << "the unsorted jobs : "<<endl;
//    printJobs(arr,n);

//    cout << "the sorted jobs according to their profits: "<<endl;
//    sort(arr,arr+n,sortProfit);
//    printJobs(arr,n);
//
    jobSequencing(arr,n);
    cout << "number of steps: " << steps << endl ; 
}
