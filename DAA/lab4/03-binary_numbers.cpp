/* 
  generate all binary strings without consequtive 1's for 4 bit system
*/

#include<iostream>
#include<chrono>
using namespace std;

void printTheArray(int arr[], int n) {
    for (int i = 0; i < n; i++) {
        cout << arr[i] << " ";
    }
    cout << endl;
}

// Function to generate all binary strings
void generateAllBinaryStrings(int n, int arr[], int i) {
    if (i == n) {
        printTheArray(arr, n);
        return;
    }
 
    // First assign "0" at ith position
    // and try for all other permutations
    // for remaining positions
    arr[i] = 0;
    generateAllBinaryStrings(n, arr, i + 1);
 
    // And then assign "1" at ith position
    // and try for all other permutations
    // for remaining positions
    arr[i] = 1;
    generateAllBinaryStrings(n, arr, i + 1);
}

int main() {
  int n = 4;
  int arr[16];

  cout <<"\n" << n << "-bit binary numbers are: \n";
  chrono::time_point<chrono::system_clock> start, end;
  start = chrono::system_clock::now();
  generateAllBinaryStrings(n,arr,0);
  end = chrono::system_clock::now();
  chrono::duration<double,std::milli> elapsed_sec = end - start;
  cout << "\nThe elapse time to generate all the strings representing binary numbers of "<< n << " bit is: " << elapsed_sec.count() << "ms "<<endl; 
}

