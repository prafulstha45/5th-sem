#include <iostream>
#include <algorithm>
#include <fmt/core.h>
using namespace std;
 
int steps=0;
// Structure for an item which stores weight and
// corresponding value of Item
struct Item {
    int value, weight;
 
    // Constructor
    Item(int value, int weight)
    {
        this->value = value;
        this->weight = weight;
    }
};
 
// Comparison function to sort Item
// according to val(profit)/weight ratio
static bool cmp(struct Item a, struct Item b)
{
    double r1 = (double)a.value / (double)a.weight;
    double r2 = (double)b.value / (double)b.weight;
    return r1 > r2;
}
 
// Main greedy function to solve problem
double fractionalKnapsack(int W, struct Item arr[], int N)
{
    // Sorting Item on basis of ratio
    sort(arr, arr + N, cmp);
 
    double finalvalue = 0.0;
 
    // Looping through all items
    for (int i = 0; i < N; i++) {
        steps++;         
        // If adding Item won't overflow,
        // add it completely
        if (arr[i].weight <= W) {
            W -= arr[i].weight;
            finalvalue += arr[i].value;
            cout << "\nAdded object, weight: " << arr[i].weight << " and profit: " << arr[i].value << " completely\n";
            cout << "\tTotal profit : " << finalvalue << "\n\tSpace left: " << W << endl; 
        }
        // If we can't add current Item,
        // add fractional part of it
        else {
            
            int partialProfit = W * ((double)arr[i].value / (double)arr[i].weight);
            finalvalue += partialProfit; 
    
            cout << "\nAdded object, weight: " << W << " and profit: " << partialProfit << " partially\n";
            cout << "\tTotal profit : " << finalvalue << "\n\tSpace left: 0" << endl; 
            break;
        }
    }
 
    // Returning final value
    return finalvalue;
}


void printItem(Item arr[],int size) {
    for(int i=0;i<size; i++) {
        cout << i << " : " << endl;
        cout <<"\t value  : "<<arr[i].value << endl;
        cout <<"\t weight : "<<arr[i].weight<< endl;
        cout <<"\t p:w ratio: " << (float)arr[i].value/(float)arr[i].weight<<endl;
    }
    cout << endl;

}

int main()
{
    int W = 50;
//    Item = {value, weight}
//    Item arr[] = { {40,10}, {30,30}, {80,20}, {70,50}};
    Item arr[] = { { 60, 10 }, { 100, 20 }, { 120, 30 } };
//    Item arr[] = {{15,5},{10,3}, {9,3}, {8,1}, {7,3} };
//    Item arr[] = { {10,1} ,{15,3}, {7,5}, {8,4},{9,1},{4,3}}; 
    int N = sizeof(arr) / sizeof(arr[0]);
 
    // Function call
    cout << "Total capacity of sack = " << W << endl;
    int maxProfit = fractionalKnapsack(W,arr,N);
    cout << "\nMax profit is : "<< maxProfit << endl; 
//    fmt::print("Max profit is: {} \n " , fractionalKnapsack(W, arr, N));
    
    cout<< "number of steps: " << steps << endl;
//    fmt::print("number of steps: {}\n",steps);
//    cout << "the sorted array is: "<<endl;
//    printItem(arr,N);
    return 0;
}
